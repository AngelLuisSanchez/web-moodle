<?php
	session_start();
	//si intenta acceder a esta url sin estar autenticado lo mandamos a login
	if(!isset($_SESSION["autenticado"])){
		header("Location: login.php");
	}else{
	
	//nos aseguramos de que solo se pueda insertar un alumno si se ha enviado el formuario
	if(isset($_POST['nombre'])){
		$_SESSION["permiteinsertar"] = 1;
	}
		
	if(isset($_SESSION["invitado"]))
		header("Location: inicio.php");

	//conectamos con la base de datos
	include 'connect.php';
	
	//obtenemos los datos introducidos
	$nombre = $_POST['nombre'];
	$apellidos = $_POST['apellidos'];
	$nota = $_POST['nota'];
	$observaciones = $_POST['observaciones'];
	
	//formatos permitidos de imagen
	$permitidos = array("image/jpg", "image/jpeg", "image/gif", "image/png");
	if(in_array($_FILES['foto']['type'], $permitidos)){
		//archivo temporal
		$foto_temporal = $_FILES['foto']['tmp_name'];
		// Leemos el contenido del archivo temporal en binario.
        $fp = fopen($foto_temporal, 'r+b');
        $foto = fread($fp, filesize($foto_temporal));
        fclose($fp);
        //Escapamos los caracteres para que se puedan almacenar en la base de datos correctamente.
        $foto = mysql_escape_string($foto);
		
	//Sentencia SQL para insertar al usuario
		if(isset($_SESSION["permiteinsertar"]) && isset($_SESSION['admin']) && isset($_SESSION['insertaruno'])){
			$ssql = "INSERT INTO student (nombre, apellidos, nota, observaciones, foto) VALUES ('$nombre', '$apellidos', '$nota', '$observaciones', '$foto')";
			unset($_SESSION["permiteinsertar"]);
			unset($_SESSION['insertaruno']);
		}
	}else{
		if(isset($_SESSION["permiteinsertar"]) && isset($_SESSION['admin']) && isset($_SESSION['insertaruno'])){
			$ssql = "INSERT INTO student (nombre, apellidos, nota, observaciones) VALUES ('$nombre', '$apellidos', '$nota', '$observaciones')";
			unset($_SESSION["permiteinsertar"]);
			unset($_SESSION['insertaruno']);
		}
	}
	
	//ejecutamos la sentencia
	mysql_query($ssql,$conn);
	
	//cerramos conexion con base de datos
	mysql_close($conn);
	
	//redireccionamos a inicio
	header ("Location: inicio.php");
	}
?>