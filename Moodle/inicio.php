<!DOCTYPE HTML>
<?php
	session_start();
	//si intenta acceder a esta url sin estar autenticado lo mandamos a login
	if(!isset($_SESSION["autenticado"])){
		header("Location: login.php");
	}else{
		
	//conectamos con la base de datos
	include 'connect.php';
	
	//Sentencia SQL para buscar al alumno 
	$ssql = "SELECT id,nombre,apellidos,foto FROM student";
	
	//Ejecutamos la sentencia 
	$rs = mysql_query($ssql,$conn);
	}
	?>

<html lang="es">

	<head>
		<title>ProjectDSW-Students</title>
		<link rel="stylesheet" href="style.css">
	</head>

	<body>	
		<div id="menu">
			<p style="text-align:center"><b>Menu</b><br></p>
			<input class="button buttonmenu" type="button" value="Home" onClick=location.href='inicio.php'>
			<input class="button buttonmenu" type="button" value="Add Student" onClick=location.href='addstudent.php' <?php if(isset($_SESSION['invitado'])) echo "disabled"?>>
		</div>		
		<div class="logout"><a href="cerrarsesion.php">Logout</a></div>

		<?php
		if(!isset($_SESSION["autenticado"])){
			header("Location: login.php");
		}else{
			while($fila = mysql_fetch_array($rs)){
				$id = $fila['id'];
				echo '<a href="mostraralumno.php?id='.$id.'"><div align="center" class="student">';
				echo "<b>".$fila['nombre']." ".$fila['apellidos']."</b><br>";
				if($fila['foto'] != NULL){
					echo '<div class="imgdiv"><img class="imgstudent" src=img.php?id='.$fila['id'].'/></div>';
				}	
				echo '</div></a>';
			}

			//cerramos conexion con base de datos
			mysql_close($conn);
		}
		?>
		
		<footer><font>Authors: David Valencia Delgado-Corredor and Angel Luis Sanchez Gomez</font></footer>
	</body>

</html>