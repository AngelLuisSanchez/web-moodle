<?php 
	session_start();
	if(isset($_SESSION["autenticado"]))
		header("Location: inicio.php");
?>
			
<!DOCTYPE HTML>

<html lang="es">

	<head>
		<title>ProyectoWeb</title>
		<link rel="stylesheet" type="text/css" href="style.css">

	</head>
	
	<body style="margin-top:200px">
		<h1>Web Systems Development</h1>

		<center>
			<form action="compruebalogin.php" method="POST" id="formulario">
				<label>User </label>
				<input type="text" placeholder="User" name="user" id="user">
				<p><label>Password </label>
				<input type="password" placeholder="Password" name="password" id="password"> </p>
				<p><input class="button" type="submit" value="Access" name="Access" style="margin-right:10px"><a class="button" href="registro.php">Sign in</a></p>
			</form>
		</center>
		<?php 
			if(isset($_SESSION["noexiste"])){
				echo '<p align="center" style="color:red">The user or password is incorrect</p>';
				unset($_SESSION["noexiste"]);
			}
		?>
		
	</body>
</html>