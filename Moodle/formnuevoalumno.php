<?php
	session_start();
	//si intenta acceder a esta url sin estar autenticado lo mandamos a login
	if(!isset($_SESSION["autenticado"]))
		header("Location: login.php");
		
	if(isset($_SESSION["invitado"]))
		header("Location: inicio.php");

	//conectamos con la base de datos
	include 'connect.php';
	
	//obtenemos id
	$id = $_GET['id'];
	
	//Sentencia SQL para obtener datos del alumno
	$ssql = "SELECT nombre, apellidos, nota, observaciones, foto FROM student WHERE id='$id'";
	$rs = mysql_query($ssql, $conn);
	$fila = mysql_fetch_array($rs);
	
	//cerramos conexion con base de datos
	mysql_close($conn);
?>
		<link rel="stylesheet" href="style.css">
		<div id="menu">
			<p style="text-align:center"><b>Menu</b><br></p>
			<input class="button buttonmenu" type="button" value="Home" onClick=location.href='inicio.php'>
			<input class="button buttonmenu" type="button" value="Add Student" onClick=location.href='addstudent.php' <?php if(isset($_SESSION['invitado'])) echo "disabled"?>>
		</div>		
		<div class="logout"><a href="cerrarsesion.php">Logout</a></div>

<form action="modificaralumno.php?id=<?php echo $id?>" method="POST" id="formulario" enctype="multipart/form-data">
		<p><label>Nombre </label></p>
		<input type="text" name="nombre" value="<?php echo $fila['nombre']?>" required>
		<p><label>Apellidos </label></p>
		<input type="text" name="apellidos" value="<?php echo $fila['apellidos']?>" required>
		<p><label>Nota </label></p>
		<input type="number" name="nota" min="0" max="10" value="<?php echo $fila['nota']?>">
		<p><label>Observaciones </label></p>
		<textarea name="observaciones" rows="5" cols"50"><?php echo $fila['observaciones']?></textarea>
		<?php if($fila['foto'] != NULL){
			echo '<p><label>Foto </label></p>';
			echo '<div><p><img src="img.php?id='.$id.' width="100px" height="100px"></p></div>';
		}?>
		<p ><input type="file" name="foto"></p>
		<p><input class="button" type="submit" value="Modify"></p>
	</form>
	<footer><font>Authors: David Valencia Delgado-Corredor and Angel Luis Sanchez Gomez</font></footer>