<?php 
	session_start();
	//si intenta acceder a esta url sin estar autenticado lo mandamos a login
	if(!isset($_SESSION["autenticado"])){
		header("Location: login.php");
	}else{
		
	if(isset($_SESSION["invitado"]))
		header("Location: inicio.php");
	}
	
	//con esta variable evitamos que se inserte mas de un alumno si enviamos el formulario mas de una vez de seguido
	$_SESSION['insertaruno'] = 1;
?>
<html>
<head>
	<title>Add Student</title>
	<link rel="stylesheet" href="style.css">
</head>
<body>
	<div id="menu">
			<p style="text-align:center"><b>Menu</b><br></p>
			<input class="button buttonmenu" type="button" value="Home" onClick=location.href='inicio.php'>
			<input class="button buttonmenu" type="button" value="Add Student" onClick=location.href='addstudent.php' <?php if(isset($_SESSION['invitado'])) echo "disabled"?>>
		</div>		
		<div class="logout"><a href="cerrarsesion.php">Logout</a></div>
		
	<form action="insertaralumno.php" method="POST" id="formulario" enctype="multipart/form-data">
		<p><label>Nombre </label></p>
		<input type="text" name="nombre" required>
		<p><label>Apellidos </label></p>
		<input type="text" name="apellidos" required>
		<p><label>Nota </label></p>
		<input type="number" name="nota" min="0" max="10">
		<p><label>Observaciones </label></p>
		<textarea name="observaciones" rows="5" cols"50"></textarea>
		<p><label>Foto </label></p>
		<input type="file" name="foto">
		<p><input class="button" type="submit" value="Add"></p>
	</form>
	<footer><font>Authors: David Valencia Delgado-Corredor and Angel Luis Sanchez Gomez</font></footer>
</body>
</html>